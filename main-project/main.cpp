#include <iostream>
#include "book_subscription.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include "processing.h"


using namespace std;
void output(book_subscription* subscription)
{

	cout << "����........: ";

	cout << subscription->reader.last_name << " ";
	;
	cout << '\n';

	cout << "�����...........: ";

	cout << subscription->author.last_name << " ";

	cout << '"' << subscription->title << '"';
	cout << '\n';

	cout << "�������.....: ";
	cout << subscription->start.year;

	cout << '\n';

	cout << "�������...: ";
	cout << subscription->finish.year;

	cout << '\n';
	cout << '\n';
}
int main()
{
	cout << "Laboratory work #1. GIT\n";
	cout << "Variant #4. Currency rates\n";
	cout << "Author: Igor Ermaschkevitsch\n";
	cout << "Group: 14s\n";

	return 0;

	setlocale(LC_ALL, "Russian");
	cout << "������������ ������ �1. GIT\n";
	cout << "������� 4�. ���� �����\n";
	cout << "�����: ���������� �����\n";
	cout << "������: 14�\n\n";
	book_subscription* subscriptions[MAX_FILE_ROWS_COUNT];
	int size;

	try
		
	{
		read("data.txt", subscriptions, size);
		cout << "***** ����� ����� *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(subscriptions[i]);
		}
		bool (*check_function)(book_subscription*) = NULL; 
		cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
		cout << "1) ������� ����� ������ �� ���� ���������� ����� ������������ (� ��������).\n";
		cout << "2) ������� ����� ������ � ������ ��������� ������, � ������� ������� ������ 2,5.\n";
		cout << "\n������� ����� ���������� ������: ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_book_subscription_by_author; // ����������� � ��������� �� ������� ��������������� �������
			cout << "***** ������� ����� ������ �� ���� ���������� ����� ������������ (� ��������). *****\n\n";
			break;
		case 2:
			check_function = check_book_subscription_by_date; // ����������� � ��������� �� ������� ��������������� �������
			cout << "***** ������� ����� ������ � ������ ��������� ������, � ������� ������� ������ 2,5. *****\n\n";
			break;
		default:
			throw "������������ ����� ������";
		}
		if (check_function)
		{
			int new_size;
			book_subscription** filtered = filter(subscriptions, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete subscriptions[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}

